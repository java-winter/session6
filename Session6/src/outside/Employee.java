package outside;

import jac.Person;

//Inherit another class in another package
public class Employee extends Person {
	
	public void doSomething() {
		name = "Bob";	
		int bobAge = getAge();
		Person.address = "dsfasdfas";
		//age = 12; age is a private field in parent class so it is not accissible
		//I have access to modify the protected fields of parent class
		// and not private
		
	}
}
