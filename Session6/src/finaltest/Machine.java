package finaltest;

public class Machine {
	
	final void sayHello() {
		System.out.println("I am a machine");
	}
}

class Car extends Machine{
	//you cannot override a final method
	
//	@Override //this is an optional feature that shows what happens like a signature 
//	void sayHello() {
//		System.out.println("I am a car");
//	}
}

class MiniCar extends Car{
//	void sayHello() {
//		System.out.println("I am a minicar");
//	}
}