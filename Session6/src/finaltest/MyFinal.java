package finaltest;

//final classes cannot be inherited
public final class MyFinal {
	String value = "important data";
	
	void doCriticalStuff() {
		
	}
}


class A {
	
	//final fields cannot be changed (Constant)
	final double PI = 3.14;
	final static String name = "A";
	static int age = 12;
	
	public void doSomething() {
		String value = "NO";

	}
	
	void doCriticalStuff() {
		System.out.println("Hacked!!!");
	}
}

class B{
	void doChange() {
		//A.name= "B";//I cannot change the value of a final field
		
		A.age = 13;
	}
}
