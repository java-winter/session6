package str;

class User {
	private String name;
	private int id;
	private String password;
	
	public User(String name, int id, String password) {
		this.name = name;
		this.id = id;
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User has the following info [name=" + name + ", id=" + id + ", password=****" +"]";
	}
	
//	@Override
//	public String toString() {
//		return "The Username is " + this.getName() + "asdfasdf";
//	}
	
	
	
}


public class App {
	
	public static void main(String args[]) {
		User user1 = new User("user1", 1, "12345");
		User user2 = new User("user2", 2, "00000");
		
		//.notation
		
		System.out.println(user1);
		System.out.println(user2);
		
	}
}
