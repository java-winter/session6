package jac;

public class Person {
	protected String name; //in the same package if we would like to give access to that filed
	private int age;
	
	protected static String address; //default, protected
	
	protected int getAge() {
		return age;
	}
}

class X{
	
}
